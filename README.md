# OpenCL Nbody Simulation #

OpenCL adaptation of this paper http://runge.math.smu.edu/Math6370/_downloads/burtscher_pingali-2011.pdf Requires OpenCL 2.0 capabilities. Tested only on an Intel HD 5500 Card.

![universe3.png](https://bitbucket.org/repo/jynrXz/images/3500153634-universe3.png)